require 'bourbon'
http_path = "."
css_dir = "."
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "js"
output_style = :compressed
# output_style = :nested
relative_assets=true
line_comments = false
# line_comments = true
sass_options = {:sourcemap => true}
