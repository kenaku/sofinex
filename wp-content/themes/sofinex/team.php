<?php
/**
* Template Name: team
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
<div class="col9 content-col last">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="col12"><?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>
				</div>
				&nbsp;
				<div class="entry-content col12">
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
					<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
				</div><!-- .entry-content -->
				</article><!-- #post-## -->
<?php endwhile; ?>
</div>
<?php get_footer(); ?>