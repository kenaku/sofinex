<?php
/*
 * Template Name: Main
 */

	get_header(); ?>

		<div class="col12"><h3 class="newsblock-title">Новости</h3></div>
	</div> <!-- row -->
	<div class="row">
	<?php
		$args = array( 'posts_per_page' => 2, 'order'=> 'ASC', 'category' => '4');
		$postslist = get_posts( $args );
		foreach ($postslist as $post) :  setup_postdata($post); ?>
			<article class="col6 newsblock-article <?php if (!next($postslist)) echo "last" ?>">
				<span class="newsblock-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
				<?php the_excerpt(); ?>
				<span class="newsblock-article-date"><?php the_time(get_option('date_format')); ?></span>
			</article>
	<?php endforeach; ?>
	</div><!-- row -->
</div><!-- container main-news-bg -->
<div class="container main-content-bg">
	<div class="row main-content">
		<!-- Программы -->
				<ul>
				<?php
					$args = array( 'post_type' => 'program', 'posts_per_page' => 7 );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
				 ?>
						<li class="program-collapsed col2 last col4s">
							<div class="tab-pane program-expanded" id="program-<?php the_id(); ?>">
								<h3 class="program-expanded-header"><?php the_title(); ?></h3>
								<div class="program-close-btn"></div>
								<?php the_content(); ?>
							</div>
								<a class="program-click" href="#program-<?php the_id(); ?>" data-toggle="tab"><span><?php the_title(); ?></span></a>
						</li>
						<?php
					endwhile;	?>
				</ul>
		<!-- Программы все -->
	</div> <!-- row -->
	<div class="row">
		<div class="col12">

			<!-- Основной текст -->
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title main-page_entry-title"><?php the_title(); ?></h2>
				<div class="entry-content">
					<?php the_content(); ?>
					 <!-- <?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?> -->
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
		</div> <!-- col12 -->
			<?php endwhile; ?>
			<!-- Основной текст все-->
			<!-- Дополнительные статьи-->
			<?php
				$pages_array = array( 105, 107 );
				foreach ($pages_array as $page_id) {
				$page_data = get_page( $page_id );
			?>
			<article class="col6 <?php if ($page_id === end($pages_array)) echo 'last' ?>">
				<h3 class="main-second-articles-header"><?php echo $page_data->post_title; ?></h3>
				<p class="main-second-articles-text"><?php echo $page_data->post_content; ?></p>

				<p class="main-second-articles-more"><a class="roll-link" href="<?php echo get_permalink( $page_id ); ?>"><span data-title="Подробнее">Подробнее</span></a></p>
			</article>
			<?php } ?>
		<!-- Дополнительные статьи всё-->
		<!-- Победители -->

		<div class="col6 main-winners">
			<h3 class="col12 incol main-winners-header">Победители</h3>
			<?php $winners = get_pages( array( 'child_of' => 15, 'sort_column' => 'menu_order', 'meta_key' => 'победы' ) ); // Берем дочерние страницы в цикл
            $i = 1;
        foreach( $winners as $winner ) {
            $win = get_post_meta($winner->ID, 'победы', true);
      ?>
			<div class="col6 incol main-winner-block  <?php if($i % 2 === 0) echo 'last' ?>">
				<h4 class="main-winner-name"><?php echo $winner->post_title; ?></h4>
				<p class="main-winner-text"><?php echo $win; ?></p>
			</div>
			<?php  $i++; }; ?>
			<div class="col12 incol"><a href="/sbornaya-rossii">Все победители</a></div>
		</div><!-- col6 main-winners -->
		<!-- Победители всё-->
		<div class="col6 last main-video">
			<div class="responsive-embed-container">
				<?php $video_link = get_post_meta(46, 'Видео',true);?>
			  <?php if( ! empty( $video_link ) ) echo "$video_link"; ?>
			</div>
		</div>
	</div> <!-- row -->
</div> <!-- container -->
<div class="container main-clubs-block">
	<div class="row">
		<h3 class="col12 main-clubs-title">Клубы в Москве</h3>
		<div id="owl-example" class="owl-carousel">
		<?php
			$args = array( 'post_type' => 'club', 'posts_per_page' => 10 );
			$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
				$count_posts = $my_query->current_post + 1;
				 ?>
					<div>
						<div class="metro-header clearfix">
							<div class="main-club-header-metro">M</div>
							<!-- <h3 class="main-club-header"><?php the_title(); ?></h3> -->
							<div class="main-club-text"><?php the_content(); ?></div>
						</div>
					</div>
				<?php
					endwhile;	?>
		</div>
	</div>
</div> <!-- container main-content-bg -->

<?php get_footer(); ?>