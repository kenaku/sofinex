<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>
<aside id="sidebar" class="sidebar col3">
	<?php wp_nav_menu( array(
	'container_class' => 'sidebar-nav',
	'theme_location'  => 'sidebar'
	 ) ); ?>
</aside>