<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>
			</div> <!-- .row -->
		</section><!-- #main -->
		<footer class="site-footer" role="contentinfo">
			<div class="container  shadow_bottom">
				<div class="row">
					<div class="col12">
						<ul class="footer-partners">
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/acronis.jpg" alt=""></a></li>
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/symantec.jpg" alt=""></a></li>
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/drweb.jpg" alt=""></a></li>
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/1c.jpg" alt=""></a></li>
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/ms.jpg" alt=""></a></li>
							<li><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/kas.jpg" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
				<div class="col12 copyright">
					<p class=>© Sofinex 2013</p>
					<div class="footer-contacts">+7(812) 336-42-68 | <a href="mailto:info@sofinex.ru">info@sofinex.ru</a></div>
				</div>
			</div>
		</footer><!-- footer -->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
<script src="<?php bloginfo( 'template_directory' ); ?>/js/functions.js"></script>
	</body>
</html>