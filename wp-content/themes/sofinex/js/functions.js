// sidebar toggle on mobile screen
document.getElementById('sidebar-menu-btn').onclick = function() {
   var e = document.getElementById('sidebar');
   if(e.style.display == 'block')
      e.style.display = 'none';
   else
      e.style.display = 'block';
  }
window.onresize = function(){
  var e = document.getElementById('sidebar');
  if (window.innerWidth >= 710)
    e.style.display = 'block';
  else
    e.style.display = 'none';
}

// ipad/iphone homescreen icon title for iOS<6
if (navigator.userAgent.match(/(iPad|iPhone|iPod)/i)) {
    document.title = document.getElementsByName('apple-mobile-web-app-title')[0].content;
}